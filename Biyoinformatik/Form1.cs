﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biyoinformatik
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            string text1 = textBox1.Text.ToUpper();  //GCTGGAAGGCAT        EQLLKALEFKL
            string text2 = textBox2.Text.ToUpper();  //GCAGAGCACG          KVLEFGY
            int text1_length = text1.Length;
            int text2_length = text2.Length;

            int gap = Convert.ToInt32(textBox3.Text);
            int match = Convert.ToInt32(textBox4.Text);
            int mismatch = Convert.ToInt32(textBox5.Text);

            char[] array_text1 = new char[text1_length];
            char[] array_text2 = new char[text2_length];

            array_text1 = text1.ToCharArray();
            array_text2 = text2.ToCharArray();

            //Skor tablosunu tanımlama
            int[,] score_table = new int[text2_length + 1, text1_length + 1];

            //Tabloya ilk değerleri doldurma
            for (int i = 0; i < text2_length + 1; i++)
            {
                for (int j = 0; j < text1_length + 1; j++)
                {
                    score_table[i, j] = 0;
                }
            }

            //Tüm tabloyu doldurma
            for (int i = 0; i < text2_length; i++)
            {
                for (int j = 0; j < text1_length; j++)
                {
                    if (array_text2[i] == array_text1[j])
                    {
                        if (score_table[i, j] == 0)
                        {
                            score_table[i + 1, j + 1] = match;
                        }
                        else
                        {
                            score_table[i + 1, j + 1] = score_table[i, j] + match;
                        }
                    }

                    else
                    {

                        int score1 = score_table[i + 1, j] + gap;
                        int score2 = score_table[i, j + 1] + gap;
                        int score3 = score_table[i, j] + mismatch;
                        int max_score;

                        if (score1 > score2 && score1 > score3)
                            max_score = score1;
                        else if (score2 > score1 && score2 > score3)
                            max_score = score2;
                        else
                            max_score = score3;

                        if (max_score > 0)
                        {
                            score_table[i + 1, j + 1] = max_score;
                        }
                        else
                        {
                            score_table[i + 1, j + 1] = 0;
                        }
                    }
                }
            }

            //En Büyük Bulma
            int table_max = 0;
            int row = 0;
            int column = 0;
            int row_temp = 0;

            for (int i = 0; i < text2_length + 1; i++)
            {
                for (int j = 0; j < text1_length + 1; j++)
                {
                    if (score_table[i, j] > table_max)
                    {
                        table_max = score_table[i, j];
                        row = i;
                        row_temp = i;
                        column = j;
                    }
                }
            }

            //Geriye gitme
            char[] text1_back = new char[text2_length];
            char[] text2_back = new char[text2_length];
            int[] rows = new int[text2_length];
            int[] columns = new int[text2_length];
            int temp = text2_length;

            while (temp > 0)
            {
                if (row != 0)
                {
                    int score1 = score_table[row, column] - score_table[row, column - 1]; //sol
                    int score2 = score_table[row, column] - score_table[row - 1, column]; //üst
                    int score3 = score_table[row, column] - score_table[row - 1, column - 1]; //sol çapraz

                    if (score1 == gap)
                    {
                        text2_back[temp - 1] = '_';
                        text1_back[temp - 1] = array_text1[column - 1];
                        rows[temp - 1] = row + 1;
                        columns[temp - 1] = column;

                        row = row + 0;
                        column = column - 1;
                        temp--;
                    }
                    else if (score2 == gap)
                    {
                        text2_back[temp - 1] = array_text2[row - 1];
                        text1_back[temp - 1] = '_';
                        rows[temp - 1] = row;
                        columns[temp - 1] = column + 1;

                        row = row - 1;
                        column = column + 0;
                        temp--;
                    }
                    else if (score3 == match)
                    {
                        text2_back[temp - 1] = array_text2[row - 1];
                        text1_back[temp - 1] = array_text1[column - 1];

                        rows[temp - 1] = row;
                        columns[temp - 1] = column;

                        row = row - 1;
                        column = column - 1;
                        temp--;
                    }
                    else if (score3 == mismatch)
                    {
                        text2_back[temp - 1] = array_text2[row - 1];
                        text1_back[temp - 1] = array_text1[column - 1];

                        rows[temp - 1] = row;
                        columns[temp - 1] = column;

                        row = row - 1;
                        column = column - 1;
                        temp--;
                    }
                }
                else
                {
                    text2_back[temp - 1] = '0';
                    text1_back[temp - 1] = '0';

                    rows[temp - 1] = 0;
                    columns[temp - 1] = 0;
                    temp = 0;
                }
            }


            //Harfler ile Tablo düzenleme
            string[,] table = new string[text2_length + 2, text1_length + 2];

            for (int i = 0; i < text2_length; i++)
            {
                table[i + 2, 0] = array_text2[i].ToString();
            }

            for (int i = 0; i < text1_length; i++)
            {
                table[0, i + 2] = array_text1[i].ToString();
            }

            for (int i = 0; i < text2_length + 1; i++)
            {
                for (int j = 0; j < text1_length + 1; j++)
                {
                    table[i + 1, j + 1] = score_table[i, j].ToString();
                }
            }


            //Tabloyu yazdırma
            for (int i = 0; i < text2_length + 2; i++)
            {
                for (int j = 0; j < text1_length + 2; j++)
                {
                    Label lbl = new Label();
                    lbl.BorderStyle = BorderStyle.Fixed3D;
                    lbl.BackColor = Color.Aqua;
                    lbl.Name = "lbl" + i + j;
                    lbl.Text = table[i, j];
                    lbl.Size = new Size(30, 20);
                    lbl.Location = new Point(30 * j, 20 * i);
                    panel1.Controls.Add(lbl);
                    for (int k = 0; k < rows.Length; k++)
                    {
                        if (lbl.Name == "lbl" + rows[k] + columns[k] || lbl.Text == table_max.ToString())
                        {
                            if (rows[k] == 0 && columns[k] == 0)
                            {
                                lbl.BackColor = Color.Aqua;
                            }
                            else
                                lbl.BackColor = Color.Red;
                        }
                    }
                }
            }


            // Metin yazdırma
            for (int i = 0; i < text2_length; i++)
            {
                if (text2_back[i] != '0')
                {
                    richTextBox2.AppendText(text2_back[i].ToString());
                }
            }
            for (int i = 0; i < text2_length; i++)
            {
                if (text1_back[i] != '0')
                {
                    richTextBox3.AppendText(text1_back[i].ToString());
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////

            string[,] table_2 = new string[text2_length + 2, text1_length + 2];
            
            for (int i = 0; i < text2_length; i++)
            {
                table_2[i + 1, 0] = array_text2[i].ToString();
            }

            for (int i = 0; i < text1_length; i++)
            {
                table_2[0, i + 1] = array_text1[i].ToString();
            }

            //Tabloya noktaları yerleştirme
            for (int i = 0; i < text2_length; i++)
            {
                for (int j = 0; j < text1_length; j++)
                {
                    if (array_text2[i] == array_text1[j])
                    {
                        table_2[i + 1, j + 1] = "*";
                    }
                }
            }

            // Komşusu olanları sayıyoruz
            int sayac = 1;
            int[,] sayaclar = new int[text2_length + 1, text1_length + 1];
            int gecici_j, gecici_i;

            for (int i = 0; i < text2_length + 1; i++)
            {
                gecici_i = i;
                for (int j = 0; j < text1_length + 1; j++)
                {
                    gecici_j = j;
                    while (table_2[i, j] != null)
                    {
                        if (table_2[i, j] == table_2[i + 1, j + 1])
                        {
                            sayac++;
                            i++;
                            j++;

                        }
                        else
                        {
                            j = gecici_j;
                            i = gecici_i;
                            break;
                        }
                    }
                    sayaclar[i, j] = sayac;
                    sayac = 1;
                }
            }

            // max olanı buluyoruz
            int table2_max = 0;
            int temp_i = 0, temp_j = 0;

            for (int i = 0; i < text2_length + 1; i++)
            {
                for (int j = 0; j < text1_length + 1; j++)
                {
                    if (sayaclar[i, j] > table2_max)
                    {
                        table2_max = sayaclar[i, j];
                        temp_i = i;
                        temp_j = j;
                    }
                }
            }


            string[] text_1 = new string[text1_length];
            string[] text_2 = new string[text2_length];
            int sayi = 0;

            while (sayi < table2_max)
            {
                text_2[sayi] = table_2[0, temp_j];
                text_1[sayi] = table_2[temp_i, 0];
                sayi++;
                temp_i++;
                temp_j++;
            }

            //Dot matris tablosu
            for (int i = 0; i < text2_length + 1; i++)
            {
                for (int j = 0; j < text1_length + 1; j++)
                {
                    Label lbl2 = new Label();
                    lbl2.BorderStyle = BorderStyle.Fixed3D;
                    lbl2.BackColor = Color.Aqua;
                    lbl2.Name = "lbl2" + i + j;
                    lbl2.Text = table_2[i, j];
                    if (table_2[i, j] == "*" && (table_2[i, j] == table_2[i + 1, j + 1] || table_2[i, j] == table_2[i - 1, j - 1]))
                    {
                        lbl2.BackColor = Color.Red;

                    }

                    lbl2.Size = new Size(30, 20);
                    lbl2.Location = new Point(30 * j, 20 * i);
                    panel2.Controls.Add(lbl2);
                }


            }

            // richTextBox lara en uzun olan dizilimi yazdırıyoruz
            for (int i = 0; i < table2_max; i++)
            {

                richTextBox1.AppendText(text_1[i].ToString());

            }
            for (int i = 0; i < table2_max; i++)
            {
                richTextBox4.AppendText(text_2[i].ToString());

            }


        }
    }
}

